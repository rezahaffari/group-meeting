# Monash NLP&ML Reading Group
---

- Location: Room 317, Building 6. Monash Clayton campus.
- Time: Thursday, 4-5:30pm

## Schedule

### ============================ 2020 ================================== ###
### 14. Information Theory ###
- Speaker: Reza, Philip
- Date 2020 Apr 09
- Reza talk is about advanced topic in Information Theory.
- Philip's will present papers about Simultaenous Machine Translation
     - [Learning to Translate in Real-time with Neural Machine Translation](https://www.aclweb.org/anthology/E17-1099.pdf)
     - [STACL: Simultaneous Translation with Implicit Anticipation and Controllable Latency using Prefix-to-Prefix Framework ](https://arxiv.org/pdf/1810.08398.pdf)
     - [Monotonic Infinite Lookback Attention for Simultaneous Machine Translation](https://arxiv.org/pdf/1906.05218.pdf)

### 13. Continual Learning ###
- Speaker: Lizhen, Reza
- Date: 2020 Apr 02
- Here are the papers:
     - [Incremental few-shot learning with attention attractor networks](https://papers.nips.cc/paper/8769-incremental-few-shot-learning-with-attention-attractor-networks.pdf)
     - [Episodic Memory in Lifelong Language Learning](https://papers.nips.cc/paper/9471-episodic-memory-in-lifelong-language-learning.pdf)
- Here is the [slides](https://drive.google.com/open?id=1-JgVWm4Uuzb3hju-ZjGtPTG5QgYI_HNo)
- Reza's talk is about information theory.
     - Here is the [slides](https://drive.google.com/open?id=1Z3T46l_Oh0tL_OZo41RdXhAelnjFzF_4)


### 12. Boosting Factual Correctness of Abstractive Summarization with Knowledge Graph ###
- Speaker: Michelle, Lizhen
- Here is the [paper](https://arxiv.org/pdf/2003.08612.pdf)
- Date: 2020 Mar 26
- Reading:
     - [Overcoming Catastrophic Forgetting With Unlabeled Data in the Wild](http://openaccess.thecvf.com/content_ICCV_2019/papers/Lee_Overcoming_Catastrophic_Forgetting_With_Unlabeled_Data_in_the_Wild_ICCV_2019_paper.pdf)
     - [Incremental few-shot learning with attention attractor networks](https://papers.nips.cc/paper/8769-incremental-few-shot-learning-with-attention-attractor-networks.pdf)
     - [Episodic Memory in Lifelong Language Learning](https://papers.nips.cc/paper/9471-episodic-memory-in-lifelong-language-learning.pdf)
     - [Experience replay for continual learning](https://papers.nips.cc/paper/8327-experience-replay-for-continual-learning.pdf)

### 11. Anchors: High-Precision Model-Agnostic Explanations ###
- Speaker: Sameen, Michelle
- Here is the [paper](https://www.aaai.org/ocs/index.php/AAAI/AAAI18/paper/view/16982/15850)
- Date: 2020 Mar 19
- Reading
     - [Extractive Summarization of Long Documents by Combining Global and Local Context](https://arxiv.org/abs/1909.08089)
     - [Unsupervised Neural Single-Document Summarization of Reviews via Learning Latent Discourse Structure and its Ranking](https://www.aclweb.org/anthology/P19-1206.pdf)
     - [BottleSum: Unsupervised and Self-supervised Sentence Summarization using the Information Bottleneck Principle](https://arxiv.org/abs/1909.07405)

### 10. Interpreting and improving natural-language processing (in machines) with natural language-processing (in the brain) ###
- Speaker: Snow, Sameen
- Here is the [paper](https://arxiv.org/pdf/1905.11833.pdf)
- Date: 2020 Mar 12
- Reading:
     - [Glass-Box: Explaining AI Decisions With Counterfactual Statements Through Conversation With a Voice-enabled Virtual Assistant](https://www.ijcai.org/Proceedings/2018/865)
     - [One Explanation Does Not Fit All: The Promise of Interactive Explanations for Machine Learning Transparency](https://arxiv.org/pdf/2001.09734.pdf)
     - [FACE: Feasible and Actionable Counterfactual Explanations](https://arxiv.org/pdf/1909.09369.pdf)
     - [bLIMEy: Surrogate Prediction Explanations Beyond LIME](https://arxiv.org/pdf/1910.13016.pdf)

### 9. LEARNING DEEP REPRESENTATIONS BY MUTUAL INFORMATION ESTIMATION AND MAXIMIZATION ###
- Speaker: Samitha, Snow
- Here is the [paper](https://arxiv.org/pdf/1808.06670.pdf)
- Date: 2020 Mar 05
- Reading:
     - [Hierarchical interpretations for neural network predictions](https://arxiv.org/pdf/1806.05337.pdf)
     - [LS-Tree: Model Interpretation When the Data Are Linguistic](https://arxiv.org/pdf/1902.04187.pdf)
     - [Towards Automatic Concept-based Explanations](https://arxiv.org/pdf/1902.03129.pdf)
     - [CoCoX: Generating Conceptual and Counterfactual Explanations via Fault-Lines](https://pdfs.semanticscholar.org/d03e/66a84b92f520235079083d3c0947b2c910e0.pdf)

### 8. Zhuang Li: Presentation Practice###
- Speaker: Zhuang Li, Samitha
- Title: Low-resource Context-dependent Semantic Parsing
- Date: 2020 Feb 27
- Reading:
     - [CONFIDENCE SCORES MAKE INSTANCE-DEPENDENT LABEL-NOISE LEARNING POSSIBLE](https://arxiv.org/pdf/2001.03772.pdf)
     - [LEARNING ROBUST REPRESENTATIONS VIA MULTI-VIEW INFORMATION BOTTLENECK](https://openreview.net/pdf?id=B1xwcyHFDr)
     - [CROSS-DOMAIN FEW-SHOT CLASSIFICATION VIA LEARNED FEATURE-WISE TRANSFORMATION](https://openreview.net/pdf?id=SJl5Np4tPr)
     - [Torchmeta: A Meta-Learning library for PyTorch](https://arxiv.org/pdf/1909.06576.pdf)
     - [PHASE TRANSITIONS FOR THE INFORMATION BOTTLENECK IN REPRESENTATION LEARNING](https://openreview.net/pdf?id=HJloElBYvB)

### 7. Trang: Presentation Practice ###
- Speaker: Trang, Reza
- Date: 2020 Feb 20
- Reading:
     - [Self-Distillation Amplifies Regularization in Hilbert Space](https://arxiv.org/abs/2002.05715)
     - [Self-training with Noisy Student improves ImageNet classification](https://arxiv.org/abs/1911.04252)
     - [UNSUPERVISED DATA AUGMENTATION FOR CONSISTENCY TRAINING](https://openreview.net/pdf?id=ByeL1R4FvS)

### 6. Parallel Machine Translation with Disentangled Context Transformer ###
- Speaker: Najam
- Date: 2020 Feb 13
- Here is the [paper](https://arxiv.org/abs/2001.05136)

### 5. Born Again Neural Networks ###
- Speaker: Xuanli, Najam
- Date: 2020 Feb 06
- Here is the [paper](https://arxiv.org/abs/1805.04770)
- Reading: 
     - [Understanding Knowledge Distillation in Non-autoregressive Machine Translation](https://openreview.net/forum?id=BygFVAEKDH)
     - [Parallel Machine Translation with Disentangled Context Transformer](https://arxiv.org/abs/2001.05136)
     - [Guiding Non-Autoregressive Neural Machine Translation Decoding with Reordering Information](https://arxiv.org/abs/1911.02215)
     - [Minimizing the Bag-of-Ngrams Difference for Non-Autoregressive Neural Machine Translation](https://arxiv.org/pdf/1911.09320.pdf)
     - [Semi-Autoregressive Training Improves Mask-Predict Decoding](https://arxiv.org/pdf/2001.08785.pdf)

### 4. Presentation Practice ###
- Speaker: Fahimeh
- Date: 2020 Jan 30

### 3. Quick paper skim ###
- Speaker: Xuanli
- Date: 2020 Jan 23
- Reading:
     - [DATASET DISTILLATION](https://arxiv.org/pdf/1811.10959.pdf)
     - [Good-Enough Compositional Data Augmentation](https://arxiv.org/pdf/1904.09545.pdf)
     - [SpotTune: Transfer Learning through Adaptive Fine-tuning](https://arxiv.org/pdf/1811.08737.pdf)
     - [Explicit Inductive Bias for Transfer Learning with Convolutional Networks](https://arxiv.org/pdf/1802.01483.pdf)
     - [Insertion Transformer: Flexible Sequence Generation via Insertion Operations](https://arxiv.org/pdf/1902.03249.pdf)
     - [Non-Monotonic Sequential Text Generation](https://arxiv.org/pdf/1902.02192.pdf)
     - [Sequence Modeling with Unconstrained Generation Orde](https://arxiv.org/pdf/1911.00176.pdf)

### 2. Iterative Search for Weakly Supervised Semantic Parsing ###
- Speaker: Narjes, Ming
- Date: 2020 Jan 16
- Here is the [paper](https://pdfs.semanticscholar.org/af17/ccbdae4cbd1b67d6ab359615c8000f8fb66f.pdf?_ga=2.70873744.1817092652.1575613264-1808984465.1570499999)
- Reading:
     - [DATA-DEPENDENT GAUSSIAN PRIOR OBJECTIVE FOR LANGUAGE GENERATION](https://openreview.net/pdf?id=S1efxTVYDr)
     - [DEEP BATCH ACTIVE LEARNING BY DIVERSE, UNCERTAIN GRADIENT LOWER BOUNDS](https://openreview.net/pdf?id=ryghZJBKPS)
     - [CAUSAL DISCOVERY WITH REINFORCEMENT LEARNING](https://openreview.net/pdf?id=S1g2skStPB)
     - [LEARNING TO BALANCE: BAYESIAN META-LEARNING FOR IMBALANCED AND OUT-OF-DISTRIBUTION TASKS](https://openreview.net/pdf?id=rkeZIJBYvr)
     - [RESTRICTING THE FLOW: INFORMATION BOTTLENECKS FOR ATTRIBUTION](https://openreview.net/pdf?id=S1xWh1rYwB)

### 1. Sparse Networks from Scratch: Faster Training without Losing Performance ###
- Speaker: Philip
- Date: 2020 Jan 09
- Here is the [paper](https://arxiv.org/pdf/1907.04840v2.pdf)

### ============================ 2019 ================================== ###

### 37. Quick Paper Skim ###
- Speaker: Reza
- 2019 November 28

### 36. Learning to Create Sentence Semantic Relation Graphs for Multi-Document Summarization ###
- Speaker: Michelle
- 2019 November 20
- Here is the [paper](https://www.aclweb.org/anthology/D19-5404.pdf)

### 35. When a Good Translation is Wrong in Context: Context-Aware Machine Translation Improves on Deixis, Ellipsis, and Lexical Cohesion ###
- Speaker: Sameen
- 2019 November 13
- Here is the [paper](https://www.aclweb.org/anthology/P19-1116.pdf)

### 34. Naver Labs Europe's Systems for the Document-Level Generation and Translation Task at WNGT 2019 ###
- Speaker: Fahimeh
- 2019 November 06
- Here is the [paper](https://arxiv.org/pdf/1910.14539.pdf)
- Her work during internship.

### 33. Measuring abstract reasoning in neural networks ###
- Speaker: Snow
- Here is the [paper](https://arxiv.org/pdf/1807.04225.pdf)
- 2019 October 30

### 32. ACL PresentationPractice ###
- Speaker: Trang
- 2019 October 23

### 31. Object Graph Translation by Language ###
- Speaker: Xuanli
- 2019 October 16
- His work during internship.

### 30. Insertion-based Decoding with automatically Inferred Generation Order ###
- Speaker: Najam
- Here is the [paper](https://arxiv.org/abs/1902.01370)
- 2019 October 09

### 29. Language Models as Knowledge Bases ###
- Speaker: Farhad
- Here is the [paper](https://arxiv.org/pdf/1909.01066.pdf)
- 2019 September 25

### 28. Presentation Practice ###
- Speaker: Narjes
- 2019 September 18
- Here is the [paper](https://arxiv.org/pdf/1910.14539.pdf)

### 27. Self-Supervised Generalisation with Meta Auxiliary Learning ###
- Speaker: Reza
- 2019 September 11
- Here is the [paper](https://arxiv.org/abs/1901.08933)

### 26. Attending to Future Tokens for Bidirectional Sequence Generation ###
- Speaker: Philip
- 2019 September 4
- Here is the [paper](https://arxiv.org/pdf/1908.05915.pdf)

### 25. ON THE VARIANCE OF THE ADAPTIVE LEARNING RATE AND BEYOND ###
- Speaker: Poorya
- 2019 August 28
- Here is the [paper](https://arxiv.org/pdf/1908.03265.pdf)

### 24. Multi-Head Attention with Diversity for Learning Grounded Multilingual Multimodal Representations ###
- Speaker: Xiaojun Chang
- 2019 August 21
- Abstract: With the aim of promoting and understanding the multilingual version of image search, 
we leverage visual object detection and propose diverse multi-head attention networks to learn grounded multilingual multimodal representations. 
Specifically, our model attends to different types of textual semantics in two languages and visual objects for aligning sentences and images. 
We introduce a new objective function which explicitly encourages attention diversity to learn an improved visual-semantic embedding space. 
We evaluate our model in the German-Image and English-Image matching tasks on the Multi30K dataset, and in the Semantic Textual Similarity task with the 
English descriptions of visual content. Results show that our model yields a significant performance gain over other methods in all of the three tasks.


### 23. ACL Recap ###
- Speaker: Reza
- 2019 August 14

### 22. Complex Question Answering over Large-scale Knowledge Bases ###
- Speaker: Devin Hua
- 2019 August 07

### 21. Analyzing Multi-Head Self-Attention: Specialized Heads Do the Heavy Lifting, the Rest Can Be Pruned ###
- Speaker: Sameen
- 2019 July 31
- Here is the [paper](https://arxiv.org/pdf/1905.09418)

### 20. Attention is not an Explanation ###
- Speaker: Snow
- 2019 July 24
- Here is the [paper](https://arxiv.org/pdf/1902.10186)

### 19. MeanSum : A Neural Model for Unsupervised Multi-Document Abstractive Summarization###
- Speaker: Ming
- 2019 July 15
- Here is the [paper](https://arxiv.org/pdf/1810.05739.pdf)

### 18. NAACL Recap ###
- Speaker: Philip
- 2019 June 27

### 17. NAACL Recap ###
- Speaker: Reza
- 2019 June 12

### 16. Unsupervised Data Augmentation ###
- Speaker: Trang
- 2019 May 27
- Here is the [paper](https://arxiv.org/abs/1904.12848)

### 15. Presentation Practice ###
- Speaker: Snow & Sameen
- 2019 May 20

### 14. NVIDIAs automatic mixed precision training to make deep learning training faster ###
- Speaker: Maggie
- 2019 May 13
- Here is the [paper](Papers/FasterDLtrainingAMP-Monash.pdf)

### 13. From Recognition to Cognition: Visual Commonsense Reasoning###
- Speaker: Narjes
- 2019 May 1
- Here is the [paper](https://arxiv.org/pdf/1811.10830.pdf)

### 12. Universal Neural Machine Translation for Extremely Low Resource Languages ###
- Speaker: Philip
- 2019 Apr 24
- Here is the [paper](https://www.aclweb.org/anthology/N18-1032)


### 11. Reinforcement Learning based Curriculum Optimization for Neural Machine Translation ###
- Speaker: Poorya
- 2019 Apr 10
- Here is the [paper](https://arxiv.org/pdf/1903.00041.pdf)


### 10. XNLI: Evaluating Cross-lingual Sentence Representations ###
- Speaker: Reza
- 2019 Apr 1
- Here is the [paper](https://arxiv.org/pdf/1809.05053v1.pdf)


### 9. Richness of conceptual structure in a moment of conscious perception: an implication from integrated information theory (IIT) of consciousness  ###
- Nao Tsuchiya 
- 2019 Mar 27
- Abstract:
     - The physical substrate of consciousness is one of the most enigmatic problems in the current science, attracting wide interests traditionally from philosophers, psychologists and neuroscientists and recently from engineers and computer scientists who work on artificial intelligence. One of the key aspects of conscious experience is its immense power in simultaneous differentiation and integration; each moment of conscious experience is highly distinguishable from any other possible experiences, yet always experienced as one unified and integrated whole.  How is this possibly supported by the brains?  In this talk, I will introduce a particularly promising theory of consciousness: integrated information theory (IIT).  I will explain its core theoretical concepts, its application to the neural recording data, and its inspiration into a novel psychological paradigm - whose complex and large dataset I hope to analyze in collaboration with computational linguists.  If time is permitted, I will also discuss a possibility of consciousness in AI from IIT perspective and its potential connection to physics (computational mechanics, information thermodynamics) and mathematics (category theory).

### 8. QuAC : Question Answering in Context  ###
- Saeed
- 2019 Mar 6
- Reading:
     - [ QuAC : Question Answering in Context ](https://arxiv.org/pdf/1808.07036.pdf)
     
### 7. Redux on the NIPS 2018 papers  ###
- Everyone
- 2019 Feb 22
- NIPS papers are listed on the [website](https://nips.cc/Conferences/2018/Schedule?type=Poster)
- List your recommended papers [here](https://docs.google.com/document/d/1bwr3SdMjOVcaAb2MeOFKtGxvM5EtN67L5wxReIcjEEk/edit?usp=sharing)

### 6. Redux on the NIPS 2018 papers  ###
- Everyone
- 2019 Feb 15
- NIPS papers are listed on the [website](https://nips.cc/Conferences/2018/Schedule?type=Poster)
- List your recommended papers [here](https://docs.google.com/document/d/1bwr3SdMjOVcaAb2MeOFKtGxvM5EtN67L5wxReIcjEEk/edit?usp=sharing)
- FIT Talk 1: Using a Single Learnable Representation for Both Plan Recognition and Planning, Clayton (16 Rainforest Walk, Lecture Theatre S3), 10-11am
- FIT Talk 2: Friendship paradox and information bias in networks, Kristina Lerman (University of Southern California), Clayton (16 Rainforest Walk, Lecture Theatre S3), 2-3pm

### 5. Redux on the ICLR 2019 papers  ###
- Everyone
- 2019 Feb 8
- ICLR papers are listed on the [website](https://openreview.net/group?id=ICLR.cc/2019/Conference)
- List your recommended papers [here](https://docs.google.com/document/d/1bwr3SdMjOVcaAb2MeOFKtGxvM5EtN67L5wxReIcjEEk/edit?usp=sharing)

### 4. Redux on the ICLR 2019 papers  ###
- Everyone
- 2019 Feb 1
- ICLR papers are listed on the [website](https://openreview.net/group?id=ICLR.cc/2019/Conference)
- List your recommended papers [here](https://docs.google.com/document/d/1bwr3SdMjOVcaAb2MeOFKtGxvM5EtN67L5wxReIcjEEk/edit?usp=sharing)

### 3. Redux on the ICLR 2019 papers  ###
- Everyone
- 2019 Jan 25
- ICLR papers are listed on the [website](https://openreview.net/group?id=ICLR.cc/2019/Conference)
- List your recommended papers [here](https://docs.google.com/document/d/1bwr3SdMjOVcaAb2MeOFKtGxvM5EtN67L5wxReIcjEEk/edit?usp=sharing)


### 2. Redux on the ICLR 2019 papers  ###
- Everyone
- 2019 Jan 18
- ICLR papers are listed on the [website](https://openreview.net/group?id=ICLR.cc/2019/Conference)
- List your recommended papers [here](https://docs.google.com/document/d/1bwr3SdMjOVcaAb2MeOFKtGxvM5EtN67L5wxReIcjEEk/edit?usp=sharing)

### 1. BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding  ###
- Reza
- 2019 Jan 11
- Reading:

     - [ BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding ](https://arxiv.org/abs/1810.04805)
     - [ BERT Explained ](https://towardsdatascience.com/bert-explained-state-of-the-art-language-model-for-nlp-f8b21a9b6270)
     - [ The Illustrated Transformer ](http://jalammar.github.io/illustrated-transformer/)

### ============================ 2018 ================================== ###

### 42. Contextual Neural Model for Translating Bilingual Multi-Speaker Conversations  ###
- Sameen
- 2018 Dec 14
- Reading:
     - [ Contextual Neural Model for Translating Bilingual Multi-Speaker Conversations ](https://arxiv.org/pdf/1809.00344.pdf)

### 41. EMNLP Experience  ###
- Ming and Xuanli
- 2018 Dec 7

### 40. Do deep generative models know what they don't know  ###
- Snow
- 2018 Nov 30
- Reading:
     - [ Do deep generative models know what they don't know ](https://arxiv.org/pdf/1810.09136.pdf)

### 39. TBA  ###
- Julian Garcia
- 2018 Nov 23
- Abstract:
     - What can (evolutionary) game theory tell us about multi-agent learning?
     - Learning in  environments with multiple agents is inherently more complex than in the standard single-agent case. Optimal policies are best responses that depend on the policies of other agents. This non-stationarity  of the problem gives rise to a number of open challenges. Can game theory help us design better learning algorithms? Game theory is the formal study of strategic interactions, and often assumes that players are rational, in the sense that they play Nash equilibria. This implies playing a best response to the best responses of other players. This assumption is strong, because computing Nash is demonstrably hard.  In evolutionary game theory (EGT),  agents do not optimise, but adjust their strategies using simple rules. There is a toolbox to predict game outcomes on the basis of this simple learning  process. I review these two literatures and try to discuss how EGT and Classic AI learning can help each other's agenda.

### 38. Master Student's Presentations  ###
- Islam/Paulo/Omar
- 2018 Nov 16

### 37. The Loss Surfaces of Multilayer Networks  ###
- Jane Gao
- 2018 Nov 9
- Reading:
     - [ The Loss Surfaces of Multilayer Networks ](https://arxiv.org/pdf/1412.0233.pdf)


### 36. No Reading Group  ###
- 2018 Nov 2

### 35. Dynamic Meta-Embeddings for Improved Sentence Representations  ###
- Trang
- 2018 Oct 26
- Reading:
     - [ Dynamic Meta-Embeddings for Improved Sentence Representations ](https://arxiv.org/pdf/1804.07983.pdf)

### 34. BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding  ###
- Xuanli
- 2018 Oct 19
- Reading:
     - [ BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding ](https://arxiv.org/pdf/1810.04805.pdf)

### 33. Fixing a Broken ELBO  ###
- Daniel
- 2018 Oct 12
- Reading:
     - [ Fixing a Broken ELBO ](https://arxiv.org/pdf/1711.00464.pdf)



### 32. Sentence Simplification with Deep Reinforcement Learning  ###
- Islam
- 2018 Oct 5
- Reading:
     - [ Sentence Simplification with Deep Reinforcement Learning ](https://arxiv.org/pdf/1703.10931.pdf)

### 31. Phrase-Based & Neural Unsupervised Machine Translation  ###
- Fahimeh
- 2018 Sep 21
- Reading:
     - [ Phrase-Based & Neural Unsupervised Machine Translation ](https://arxiv.org/pdf/1804.07755.pdf)



### 30. Model-Agnostic Meta-Learning for Fast Adaptation of Deep Networks  ###
- Ming
- 2018 Sep 7
- Reading:
     - [ Model-Agnostic Meta-Learning for Fast Adaptation of Deep Networks ](https://arxiv.org/pdf/1703.03400.pdf)



### 29. Learning to Speed Up Structured Output Prediction  ###
- Najam
- 2018 Aug 31
- Reading:
     - [ Learning to Speed Up Structured Output Prediction ](http://proceedings.mlr.press/v80/pan18b/pan18b.pdf)


### 28. Neural NILM: Deep Neural Networks Applied to Energy Disaggregation  ###
- Omar
- 2018 Aug 24
- Reading:
     - [ Neural NILM: Deep Neural Networks Applied to Energy Disaggregation ](https://arxiv.org/pdf/1507.06594.pdf)

### 27. Deep Learning for Medical Image Processing  ###
- Zongyuan
- 2018 Aug 17

### 26. "It's not just what you say, it's how you say it!": Recognizing Emotions in Spoken Dialogue  ###
- Leimin
- 2018 Aug 10

### 25. ICML Redux  ###
- [List of Papers](https://docs.google.com/document/d/17OIwE_JFrGZDBWlPalZSJrLHSxh-v_ckGuW3iWbYgVw/edit)
- 2018 Aug 3

### 24. ACL Redux  ###
- [List of Papers](https://docs.google.com/document/d/1VtW3qmVMvAJZpmCyZ6sVlF2gkKCvopMh5-EnqiijPQ0/edit)
- 2018 July 27

### 23. Practice Talks for SIGDIAL/Progress Review/ACL ###
- Ingrid (Interpreting spoken referring expressions in physical settings) 
- Poorya (Progress Review Seminar)
- Ming (Learning How to Actively Learn: A Deep Imitation Learning Approach)
- 2018 July 9
     
### 22. Practice Talks for ACL 2018 ###
- Poorya, Sameen, Ming
- 2018 July 2
- Reading:
     - [ Adaptive Knowledge Sharing in Multi-Task Learning: Improving Low-Resource Neural Machine Translation ]
     - [ Document Context Neural Machine Translation with Memory Networks ]
     - [ Learning How to Actively Learn: A Deep Imitation Learning Approach ]

### 21. Bandit Structured Prediction for Neural Sequence-to-Sequence Learning ###
- Paulo
- 2018 June 25
- Reading:
     - [ Bandit Structured Prediction for Neural Sequence-to-Sequence Learning ](Papers/Kreutzer-et-al.-2017.pdf)

### 20. Meta Multi-Task Learning for Sequence Modeling ###
- Poorya
- 2018 June 18
- Reading:
     - [ Meta Multi-Task Learning for Sequence Modeling ](Papers/17140-77564-1-PB.pdf)

### 19. NAACL highlights ###
- Reza
- 2018 June 11

### 18. Automatic Post-Editing (Practice Talk) ###
- Trang
- 2018 June 4
     
### 17. No Reading Group ###
- 2018 May 28

### 16. No Reading Group ###
- 2018 May 21

### 15. Learning to Remember Translation History with a Continuous Cache ###
- Sameen
- 2018 May 14
- Reading:
     - [ Learning to Remember Translation History with a Continuous Cache ](https://arxiv.org/pdf/1711.09367.pdf)
     
### 14. An Overview of AI ###
- Snow
- 2018 May 7
- Reading:
     - [ An Overview of AI ](https://www.darpa.mil/attachments/AIFull.pdf)
     
### 13. Attention Is All You Need (Location this week: 14 Rainforest Walk, Seminar room G12A) ###
- Trang
- 2018 April 30
- Reading:
     - [ Attention Is All You Need  ](https://arxiv.org/pdf/1706.03762.pdf)

### 12. Get To The Point: Summarization with Pointer-Generator Networks (Location this week: 14 Rainforest Walk, Seminar room G12A) ###
- Xuanli
- 2018 April 23
- Reading:
     - [ Get To The Point: Summarization with Pointer-Generator Networks ](https://arxiv.org/pdf/1704.04368.pdf)

### 11. Identifying Semantic Divergences in Parallel Text without Annotations  ###
- Reza
- 2018 April 16
- Reading:
     - [ Identifying Semantic Divergences in Parallel Text without Annotations ](https://arxiv.org/pdf/1803.11112.pdf)

### 10. The visual cortex as a deep neural network  ###
- Hsin-Hao
- 2018 April 9

### 9. Variational Inference  ###
- Reza
- 2018 March 26
- Reading:
     - [  https://github.com/philschulz/VITutorial/blob/master/modules/M0_Intro/M0_Intro.pdf ]
     - [  https://github.com/philschulz/VITutorial/blob/master/modules/M1_Basics/M1_Basics.pdf ]
     - [  https://github.com/philschulz/VITutorial/blob/master/modules/M3_DiscreteVariables/M3_DiscreteVariables.pdf ]
     - [  https://github.com/philschulz/VITutorial/blob/master/modules/M6_DeepGenerativeModels/M6_DeepGenerativeModels.pdf ]
     - [  http://nlp.chonbuk.ac.kr/PGM/slides_other/variational_approximation_methods.pdf]

### 8. GANs  ###
- Fahimeh
- 2018 March 5
- Reading:
     - [ Generative Adversarial Nets ](Papers/GAN.pdf)
     - [ MaskGAN: Better Text Generation via Filling in the _________ ](Papers/Mask GAN.pdf)
     
### 7. Evolution of Historical Text  ###
- Diptesh
- 2018 February 26

### 6. Dual Supervised Learning  ###
- Ming
- 2018 February 12
- Reading:
     - [ Dual Supervised Learning ](Papers/1707.00415.pdf)
     
### 5. Training Neural Networks Without Gradients: A Scalable ADMM Approach  ###
- Najam
- 2018 February 5
- Reading:
     - [ Training Neural Networks Without Gradients: A Scalable ADMM Approach ](Papers/1605.02026.pdf)
     
### 4. Evaluating Discourse Phenomena in Neural Machine Translation  ###
- Sameen
- 2018 January 29
- Reading:
     - [ Evaluating Discourse Phenomena in Neural Machine Translation ](Papers/1711.00513.pdf)

### 3. Deep Compositional Question Answering with Neural Module Networks  ###
- Narjes
- 2018 January 22
- Reading:
     - [ Deep Compositional Question Answering with Neural Module Networks ](Papers/1511.02799.pdf)

### 2. Reporting Score Distributions Makes a Difference: Performance Study of LSTM-networks for Sequence Tagging  ###
- Daniel
- 2018 January 15
- Reading:
     - [ Reporting Score Distributions Makes a Difference: Performance Study of LSTM-networks for Sequence Tagging ](Papers/D17-1035.pdf)
     
### 1. Adversarial Examples for Evaluating Reading Comprehension Systems  ###
- Reza
- 2018 January 8
- Reading:
     - [ Adversarial Examples for Evaluating Reading Comprehension Systems ](Papers/1707.07328.pdf)
     
### ============================ 2017 ================================== ###

### 33. Dialogue Systems  ###
- 2017 December 11
- Quan (Practice talk for Mid-Candidature)
- Najam (continued tutorial from last week)
     - http://www.cs.toronto.edu/~slwang/primal-dual.pdf

### 32. Primal-Dual Algorithm  ###
- Najam
- 2017 December 4
- Venue: Room 145 
- Reading:
     - http://www.cs.toronto.edu/~slwang/primal-dual.pdf

### 31. A Neural Framework for Generalized Topic Models  ###
- Ethan
- 2017 November 27
- Reading:
     - [ A Neural Framework for Generalized Topic Models ](Papers/1705.09296.pdf)
     
### 30. A Day Long Tutorial on Variational Inference and Deep Generative Models ###
- Philip Schulz (PhD visitor from University of Amsterdam)
- 2017 November 16 
- Room 145
- Sessions:
    - Basics of Variational Inference: 10:00am-11:30am
    - Deep Generative Models: 2:30pm-4:00pm
- Reading:
     - [ Tutorial Github Page ](https://github.com/philschulz/VITutorial)

### 29. Early Visual Concept Learning with Unsupervised Deep Learning ###
- Bo
- 2017 November 6
- Reading:
     - [ Early Visual Concept Learning with Unsupervised Deep Learning ](Papers/1234.pdf)

### 28. Statistical Models for Inferring Tumour Heterogeneity ###
- Shams
- 2017 October 30
- Pre-submission Practice Talk
    
### 27. Survey on Neural Approach to Automatic Post Editing Task  ###
- Trang
- 2017 October 23
     
### 26.  Generative and Discriminative Text Classification with RNN  ###
- Ming
- 2017 October 16
- Reading:
     - [ Generative and Discriminative Text Classification with Recurrent Neural Networks ](Papers/1703.01898.pdf)

### 25. Growing a Neural Network for Multiple NLP Tasks  ###
- Poorya
- 2017 October 9
- Reading:
     - [ A Joint Many-Task Model: Growing a Neural Network for Multiple NLP Tasks ](Papers/1611.01587.pdf)

### 24. 2 talks (NER with LSTM) and (Context Dependent RNN for Dialog Systems) ###
- Quan
- 2017 October 2
- Readings:
     - [ Named Entity Recognition with Stack Residual LSTM and Trainable Bias Decoding ](Papers/1706.07598.pdf)
     - Context Dependent Additive Recurrent Neural Network for Dialog Systems

### 23. Reinforcement Learning for Bandit Neural Machine Translation with Simulated Human Feedback  ###
- Daniel
- 2017 September 25
- Reading:
     - [ Reinforcement Learning for Bandit Neural Machine Translation with Simulated Human Feedback ](Papers/D17-1153.pdf)

### 22. Incorporating Context in Neural Machine Translation  ###
- Sameen
- 2017 September 18
- Readings:
     - [ Context Gates for Neural Machine Translation ](Papers/Q17-1007)
     - [ Exploiting Cross-Sentence Context for Neural Machine Translation ](Papers/1704.04347.pdf)
     
### 21. End-to-end optimization of goal-driven and visually grounded dialogue systems  ###
- Narjes
- 2017 September 11
- Reading:
     - [ End-to-end optimization of goal-driven and visually grounded dialogue systems ](Papers/1703.05423.pdf)

### 20. Embedding Senses via Dictionary Bootstrapping  ###
- Ehsan
- 2017 September 4
- Reading:
     - [ Embedding Senses via Dictionary Bootstrapping ](Papers/135.pdf)

### 19. Towards Decoding as Continuous Optimisation in Neural Machine Translation  ###
- Vu (Practice Talk for EMNLP)
- 2017 August 28
- Reading:
     - [ Towards Decoding as Continuous Optimisation in Neural Machine Translation ](Papers/1701.02854.pdf)

### 18. Compressed Nonparametric Language Modelling ###
- Ehsan (Practice Talk for IJCAI)
- 2017 August 21
- Reading:
     - [ Compressed Nonparametric Language Modelling ](Papers/0376.pdf)
     
### 17. Continuous Revision of Combinatorial Structures ###
- Poorya
- 2017 July 31
- Reading:
     - [ Sequence to Better Sequence: Continuous Revision of Combinatorial Structures ](Papers/mueller17a.pdf)

### 16.  Neural Discourse Structure for Text Categorization ###
- Ming
- 2017 July 24
- Reading:
     - [  Neural Discourse Structure for Text Categorization ](Papers/1702.01829.pdf)

### 15. Leveraging Node Attributes for Incomplete Relational Data ###
- Ethan (Practice Talk for ICML)
- 2017 July 17
- Reading:
     - [ Leveraging Node Attributes for Incomplete Relational Data ](Papers/1706.04289.pdf)

### 14. A Statistically Scalable Method for Exploratory Analysis of High-Dimensional Data ###
- Shams
- 2017 July 3
- Reading:
     - [ Under Submission in ICDM](Papers/TBD.pdf)

### 13. Neighborhood Contrast Clustering ###
- Bo
- 2017 June 26
- Reading:
     - [ Under Submission in ICDM](Papers/TBD.pdf)

### 12. Neural Transduction of Complex Discrete Structures for Machine Translation ###
- Poorya
- 2017 June 19
- Confirmation Seminar Practice Talk

### 11. Context in Neural Machine Translation ###
- Sameen
- 2017 May 29
- Readings:
     - [ Exploiting Cross-Sentence Context for Neural Machine Translation ](Papers/1704.04347.pdf)
     - [ Does Neural Machine Translation Benefit from Larger Context? ](Papers/1704.05135.pdf)

### 10. BlackOut: Speeding up Recurrent Neural Network & Tree-to-Sequence Attentional MT ###
- Poorya
- 2017 May 22
- Readings:
     - [ BlackOut: Speeding up Recurrent Neural Network Language Models With Very Large Vocabularies ](Papers/1511.06909.pdf)
     - [ Tree-to-Sequence Attentional Neural Machine Translation ](Papers/1603.06075.pdf)
     - [ Complementary Sum Sampling for Likelihood Approximation in Large Scale Classification ](Papers/AISTATS2017.pdf)

### 9. Aspect-augmented Adversarial Networks for Domain Adaptation ###
- Ming
- 2017 May 15
- Readings:
     - [ Aspect-augmented Adversarial Networks for Domain Adaptation ](Papers/1701.00188.pdf)
     - [ Domain-Adversarial Training of Neural Networks ](Papers/1505.07818.pdf)

### 8. Online Segment to Segment Neural Transduction ###
- Daniel
- 2017 May 8
- Reading:
     - [ Online Segment to Segment Neural Transduction ](Papers/D16-1138.pdf)


### 7. Scalable Bayesian Learning of Recurrent Neural Networks for Language Modeling ###
- Ehsan
- 2017 May 1
- Reading:
     - [ Scalable Bayesian Learning of Recurrent Neural Networks for Language Modeling ](Papers/1611.08034v1.pdf)


### 6. Graph Convolutional Encoders for Syntax-aware Neural Machine Translation ###
- Reza
- 2017 April 24
- Reading:
     - [ Graph Convolutional Encoders for Syntax-aware Neural Machine Translation](Papers/Bastings2017.pdf)

### 5. Trainable Greedy Decoding for Neural Machine Translation ###
- Quan
- 2017 March 13
- Reading:
     - [Trainable Greedy Decoding for Neural Machine Translation](Papers/1702.02429.pdf) 

### 4.  Deep Forest: Towards An Alternative to Deep Neural Networks ###
- Bo
- 2017 March 06
- Reading:
     - [Deep Forest: Towards An Alternative to Deep Neural Networks](Papers/zhou2017.pdf) 


### 3. Probabilistic Graphical Models ###
- Reza
- 2017 February 24
- Reading:
     - [Chapter 8 of PRML](Papers/Bishop-PRML-sample.pdf)
     - [Slides 1](Papers/chapter8a_slides.pdf) and [Slides 2](Papers/chapter8b_slides.pdf)

### 2. Probabilistic Graphical Models ###
- Reza
- 2017 February 10
- Reading:
     - [Chapter 8 of PRML](Papers/Bishop-PRML-sample.pdf) 
     - [Slides 1](Papers/chapter8a_slides.pdf) and [Slides 2](Papers/chapter8b_slides.pdf)

### 1. Compressed Nonparametric Language Modelling ###
- Ehsan
- 2017 February 03
- Reading:
     - [Compressed Nonparametric Language Modelling](Papers/TBD) 

### ============================ 2016 ================================== ###

### 16. Relaxed Optimization Framework for Decoding Neural Translation Models ###
- Vu
- 2016 December 16
- Reading:
     - [Towards A Flexible Relaxed Optimization Framework for Decoding Neural Translation Models](Papers/1701.02854.pdf) 

### 15. Neural Models for Structured Prediction ###
- Sameen
- 2016 December 09
- Confirmation Seminar Practice Talk

### 14. Speed-Constrained Tuning for Statistical Machine Translation Using Bayesian Optimization###
- Daniel
- 2016 November 25
- Reading:
     - [Speed-Constrained Tuning for Statistical Machine Translation Using Bayesian Optimization](Papers/DanielNAACL2016.pdf) 

### 13. Persian-Spanish Machine Translation through a Pivot Language ###
- Benyamin
- 2016 November 18
- Reading:
     - [Persian-Spanish Low-Resource Statistical Machine Translation through English as Pivot Language](Papers/Benyamin.pdf) 

### 12. Neural Network with Dynamic External Memory ###
- Quan
- 2016 November 11
- Reading:
     - [Hybrid Computing Using a Neural Network with Dynamic External Memory](Papers/TBD) 


### 11.  Risk Minimization of Graphical Model Parameters ###
- Sameen
- 2016 October 28
- Reading:
     - [Empirical Risk Minimization of Graphical Model Parameters Given
Approximate Inference, Decoding, and Model Structure](Papers/stoyanov11a.pdf)

### 10. Infinite-order Language Modelling with Compressed Suffix Trees ###
- Ehsan (Practice Talk for TACL/EMNLP)
- 2016 October 14
- Reading:
     - [Fast, Small and Exact: Infinite-order Language Modelling with Compressed Suffix Trees](Papers/TACL2016.pdf)

### 9. Inference of Shallow-transfer Machine Translation Rules ###
- Parya
- 2016 October 07
- Reading:
     - [A generalised alignment template formalism and its application to the inference of shallow-transfer machine translation rules from scarce bilingual corpora](Papers/Parya.pdf) 

### 8. Learning Deep Structured Models ###
- Reza
- 2016 September 02
- Reading:
     - [Learning Deep Structured Models](Papers/chenb15.pdf)

### 7. Inner Attention based Recurrent Neural Networks for Answer Selection ###
- Quan
- 2016 August 25
- Reading:
     - [Inner Attention based Recurrent Neural Networks for Answer Selection](Papers/P16-1122.pdf)

### 6.Globally Normalized Transition-Based Neural Networks ###
- Sameen
- 2016 August 17
- Reading:
     - [Globally Normalized Transition-Based Neural Networks](Papers/1603.06042v2.pdf)

### 5. Variational inference for Monte Carlo objectives ###
- Ehsan
- 2016 August 12
- Reading:
     - [Variational inference for Monte Carlo objectives](Papers/1602.06725.pdf)

### 4. Neural Variational Inference and Learning in Belief Networks ###
- Ehsan
- 2016 August 05
- Reading:
     - [Neural Variational Inference and Learning in Belief Networks](Papers/1402.0030.pdf)

### 3. Ask Me Anything: Dynamic Memory Networks for Natural Language Processing ###
- Sameen
- 2016 July 29
- Reading:
     - [Ask Me Anything: Dynamic Memory Networks for Natural Language Processing](Papers/1506.07285v5.pdf)

### 2. Partition Functions ###
- Ehsan
- 2016 May 27
- Reading(s):
     - [Quick Training of Probabilistic Neural Nets by Importance Sampling](Papers/BengioAISTAT2003.pdf) 
     - [A fast and simple algorithm for training neural probabilistic Language Models](Papers/MnihICML2012.pdf)

### 1. Training Products of Experts by Minimizing the Contrastive Divergence ###
- Ehsan
- 2016 May 20
- Reading:
     - [Training Products of Experts by Minimizing the Contrastive Divergence](Papers/HintonNeuralComputation2002.pdf)
